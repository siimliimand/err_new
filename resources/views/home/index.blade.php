@extends('layouts.default')

@section('title', 'Home')

@section('meta.title', 'Home')

@section('meta.description', 'Home meta description')

@section('content')

    @include('news.list')
    
    @include('home.sidebar')
    
@endsection