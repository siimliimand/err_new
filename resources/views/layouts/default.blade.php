<html>
    <head>
        <title>{{ $appName }} - @yield('title')</title>
        <meta name="title" content="{{ $appName }} - @yield('meta.title')" />
        <meta name="description" content="@yield('meta.description')" />
        <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />
        <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
        @stack('styles')
    </head>
    <body>
        <div class="container">
            <header id="header">
                <a id="logo" href="{{ url('') }}">
                    {{ $appName }}
                </a>
            </header>
            
            <div class="content">
                @yield('content')
            </div>
        </div>
        
        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
        @stack('scripts')
    </body>
</html>