<div class='list-item'>
    <div class='title'>
        <a href='{{ $link }}' target='_blank' title='{{ $title }}'>{{ $title }}</a>
    </div>
    <div class='date'>{{ $date }}</div>
    <div class='description'>
        {{ $description }}
    </div>
</div>