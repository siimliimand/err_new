@push('scripts')
    <script type="text/javascript" src="{{ asset('js/news.list.js') }}"></script>
@endpush

<section id="news-list-container" class="main-content">
    <h2>{{ $title }}</h2>
    
    <div class="news-list">
        @foreach($news AS $item)
            @component('news.list_item', ['title' => $item->title, 'link' => $item->link, 'date' => $item->date, 'description' => $item->description])
                
            @endcomponent
        @endforeach
    </div>
</section>
