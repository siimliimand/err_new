(function($) {
    var News = function() {
        this.containerClass = '.news-list';
        this.isLoading = false;
        this.page = 1;
    };
    
    News.prototype.build = function() {
        var self = this;
        $(document).on('scroll', function() {
            self.onScroll(self);
        });
    };
    
    News.prototype.onScroll = function(self) {
        if(self.isLoading) {
            return false;
        }
        var windowHeight = $(window).height();
        var documentHeight = $(document).height();
        var top = $(document).scrollTop();
        if(windowHeight === documentHeight) {
            windowHeight = screen.height;
        }
        
        if(documentHeight - top - windowHeight - 500 < 0) {
            self.addItems();
        }
    };
    
    News.prototype.addItems = function() {
        var self = this;
        this.getItems(function(data) {
            if(typeof data.html === 'undefined' || data.html === "") {
                self.isLoading = true;
                return false;
            }
            
            $(self.containerClass).append(data.html);
        });
    };
    
    News.prototype.getItems = function(callback) {
        var self = this;
        this.isLoading = true;
        this.page ++;
        
        $.get('news', {
            page: this.page
        }, function(data) {
            self.isLoading = false;
            callback(data);
        });
    };
    
    var news = new News();
    news.build();
    
    
})(jQuery);