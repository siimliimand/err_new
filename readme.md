
# Err uudised

### Nõuded
- Php >= 7
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

### Installeerimine

Kui failid on kloonitud

```sh
composer install
```

Millegi pärast ta enam .env faili ei tee, seega tuleb nimetada .env.example .env failiks
```sh
rename .env.example .env
```

Nüüd seadistame andmebaasi.
Sellekst tuleb muuta .env faili või vastavalt sellele andmebaasi

```sh
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=err_news
DB_USERNAME=err
DB_PASSWORD=secret
```

Järgmisena lisame andmebaasi tabelid

```sh
php artisan migrate
```

Selleks, et süsteem saaks uudiseid kuvada, tuleb need importida [RSS](http://www.err.ee/rss).
Importida võiks kasutades cron-i aga hetkel teeme päringu peale lehe külastamist.
Selleks, et leht importimse tõttu aeglaseks ei läheks teeme seda taustal.

```sh
php artisan queue:work
```