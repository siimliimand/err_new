<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\NewsRepository;
use App\Contracts\NewsRepositoryInterface;

class NewsServiceProvider extends ServiceProvider
{
    
    protected $defer = true;

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(NewsRepositoryInterface::class, function(){
            return new NewsRepository();
        });
    }
    
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [NewsRepositoryInterface::class];
    }

}