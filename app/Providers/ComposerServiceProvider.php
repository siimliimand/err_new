<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('layouts.default', 'App\Http\ViewComposers\LayoutComposer');
        View::composer('news.list', 'App\Http\ViewComposers\NewsListComposer');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        
    }

}
