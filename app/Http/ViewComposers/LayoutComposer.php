<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class LayoutComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('appName', env('APP_NAME'));
    }
}