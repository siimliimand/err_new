<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Contracts\NewsRepositoryInterface AS Repository;

class NewsListComposer
{
    /**
     * The user repository implementation.
     *
     * @var Repository
     */
    protected $repository;

    /**
     * Create a new news list composer.
     *
     * @param  NewsRepository  $repository
     * @return void
     */
    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('title', 'News');
        $view->with('news', $this->repository->getItems());
    }
}