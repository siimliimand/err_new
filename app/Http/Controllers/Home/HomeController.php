<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Jobs\ErrRssImporter;

class HomeController extends Controller
{

    public function index()
    {
        // Päris lahenduse puhul kasutakse selle asemel cron-i
        dispatch(new ErrRssImporter());
        
        return view('home.index');
    }

}
