<?php

namespace App\Http\Controllers\News;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\NewsRepositoryInterface AS Repository;

class NewsController extends Controller
{

    /**
     * @var News
     */
    private $repository;

    public function __construct(Repository $repository)
    {

        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $page = $request->get('page');
        if ($page <= 1) {
            return \response::json([]);
        }
        
        $items = $this->repository->getItems($page);
        $html = "";
        foreach ($items AS $item) {
            $view = view('news.list_item');
            $fields = $item->getFillable();
            foreach ($fields AS $field) {
                $value = $item->$field;
                $view->with($field, $value);
            }
            $html .= $view->render();
        }
        return \Response::json(['html' => $html]);
    }

}
