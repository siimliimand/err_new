<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use DateTime;
use willvincent\Feeds\Facades\FeedsFacade AS Feeds;
use App\Repositories\NewsRepository AS Repository;

class ErrRssImporter implements ShouldQueue
{

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    const CACHE_KEY = 'err_rss_import';
    const CACHE_TIME = 10;
    const RSS_URL = 'http://err.ee/rss';

    protected $repository;

    public function __construct()
    {
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Repository $repository)
    {
        if (!Cache::has(static::CACHE_KEY)) {
            $this->init($repository);
            $this->import();
            $expiresAt = Carbon::now()->addMinutes(static::CACHE_TIME);
            Cache::put(static::CACHE_KEY, true, $expiresAt);
        }
    }

    protected function import()
    {
        $feed = Feeds::make(static::RSS_URL);
        $items = $feed->get_items();

        if (is_array($items)) {
            foreach ($items AS $item) {
                $data = $this->getNewsData($item);
                
                if($this->repository->slugExists($data['slug'])) {
                    break;
                }
                
                $this->repository->create($data);
            }
        }
    }

    protected function getNewsData(\SimplePie_Item $item)
    {
        $date = DateTime::createFromFormat('d F Y, g:i a', $item->get_date());
        $date->modify("+3 hours");
        $title = $item->get_title();
        $data = [
            'title' => $title,
            'slug' => str_slug($title),
            'description' => $item->get_description(),
            'link' => $item->get_link(),
            'category' => $item->get_category()->get_term(),
            'date' => $date->format('Y-m-d H:i:s')
        ];

        return $data;
    }

    protected function init(Repository $repository)
    {
        $this->repository = $repository;
    }

}
