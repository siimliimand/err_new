<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class News extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'slug', 'description', 'link', 'category', 'date'];
    
    public function getDate()
    {
        return new DateTime($this->date);
    }

}
