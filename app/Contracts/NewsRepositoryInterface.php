<?php

namespace App\Contracts;

use App\Contracts\RepositoryInterface;

interface NewsRepositoryInterface extends RepositoryInterface
{

    public function findLast();
    
    public function slugExists($slug);
    
    public function getItems($page = 1);
}
