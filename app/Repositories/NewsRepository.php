<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use App\Contracts\NewsRepositoryInterface AS NewsContract;
use App\Repositories\Repository;
use App\Models\News;

class NewsRepository extends Repository implements NewsContract
{

    const CACHE_GET_ITEMS = 'news_get_items_';
    const CACHE_TIME = 1;
    
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return News::class;
    }
    
    /**
     * @return mixed
     */
    public function findLast()
    {
        return $this->model->orderBy('date', 'desc')->first();
    }
    
    /**
     * 
     * @param string $slug
     * @return tmixed
     */
    public function slugExists($slug)
    {
        $model = $this->findBy('slug', $slug, ['id']);
        return $model !== null;
    }
    
    /**
     * 
     * @param string $page
     * @return tmixed
     */
    public function getItems($page = 1, $columns = array('*'), $perPage = 10)
    {
        $cacheKey = static::CACHE_GET_ITEMS . $page . "_" . implode("", $columns) . $perPage;
        $items = Cache::get($cacheKey, function() use ($page, $columns, $perPage, $cacheKey) {
            if($page == 1) {
                $items = $this->model->take($perPage)->orderBy('date', 'desc')->get($columns);
            } else {
                $items = $this->model->skip($page * $perPage)->take($perPage)->orderBy('date', 'desc')->get($columns);
            }
            
            if(count($items) > 0) {
                $expiresAt = Carbon::now()->addMinutes(static::CACHE_TIME);
                Cache::put($cacheKey, $items, $expiresAt);
            }
            
            return $items;
        });
        
        return $items;
    }

}
